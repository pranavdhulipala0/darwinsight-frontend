import { useEffect, useState } from "react";
import { Route, Routes } from "react-router-dom";
import "./index.css";
import Login from "./components/Landing/Login";
import Home from "./components/Landing/Home";
import LandingPage from "./components/Landing/LandingPage";

function App() {
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  async function verifyLogin() {
    if (isLoggedIn) {
      try {
        const response = await fetch(
          "http://localhost:5000/api/auth/verify-user",
          {
            method: "POST",
            credentials: "include",
          }
        );

        if (response.status === 200) {
          console.log("User is authenticated");
          const data = await response.json();
          setIsLoggedIn(true);
          console.log(data);
          sessionStorage.setItem("tenantName", data.user.tenantName);
          sessionStorage.setItem("tenantId", data.user.tenantId);
        } else {
          console.log("User is not authenticated");
        }
      } catch (error) {
        console.error("Error:", error);
      }
    }
  }

  useEffect(() => {
    verifyLogin();
  }, []);

  return (
    <div>
      <Routes>
        {sessionStorage.getItem("tenantName") ? (
          <>
            <Route path="/home" element={<Home />} />
            <Route
              path="/login"
              element={
                <LandingPage
                  isLoggedIn={false}
                  setIsLoggedIn={setIsLoggedIn}
                />
              }
              />
            <Route
              path="/"
              element={
                <LandingPage
                  isLoggedIn={isLoggedIn}
                  setIsLoggedIn={setIsLoggedIn}
                />
              }
            />
          </>
        ) : (
          <>
            <Route
              path="*"
              element={
                <LandingPage
                  isLoggedIn={false}
                  setIsLoggedIn={setIsLoggedIn}
                />
              }
            />
             
          </>
        )}
      </Routes>
    </div>
  );
}

export default App;
