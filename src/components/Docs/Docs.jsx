import React from "react";
import WordTree from "../Services/Visualization";
import CodeComponent from "../Common/CodeComponent";
import { Tabs } from "flowbite-react";
import { HiAdjustments, HiClipboardList, HiUserCircle } from "react-icons/hi";
import { MdDashboard } from "react-icons/md";
import MetricDocs from "./MetricDocs";

function Docs() {

  const metricViews = [{name:"Count Metrics", type:"COUNTMETRICS"},
  {name:"Search Metrics", type:"SEARCHMETRICS"},
  {name:"Geo Metrics", type:"GEOMETRICS"},{name:"Performance Metrics", type:"PERFORMANCEMETRICS"},]
  const docs = [
    {
      codeTitle: "Fetch your Tenant Data",
      requestType: "GET",
      codeValue: `{
  tenantId:[yourTenantId]
}`,
    },
  ];

  return (
    <div className="flex flex-1 flex-col px-4 ">
      <section className="bg-gray-100 border-1 rounded-xl dark:bg-gray-900">
        <div className="px-4 mx-auto max-w-screen-xl text-center py-8">
          <h1 className="mb-4 text-4xl font-extrabold tracking-tight leading-none text-gray-900 md:text-5xl lg:text-6xl dark:text-white">
            Check out our API Docs
          </h1>
          <p className="mb-3 text-lg font-normal text-gray-500 lg:text-xl sm:px-16 lg:px-48 dark:text-gray-400">
            Find snippets with ease & paste them into your source code.
          </p>
          <div className="flex flex-col space-y-4 sm:flex-row sm:justify-center sm:space-y-0"></div>
        </div>
      </section>
      <div className="flex mx-auto my-2 w-full justify-center">
      <Tabs aria-label="Default tabs" className="flex justify-center">
      {metricViews.map((item) => (
        <Tabs.Item key={item} title={item.name} icon={HiAdjustments}>
          <MetricDocs metric={item.type} />
        </Tabs.Item>
      ))}
      </Tabs>
      </div>
    </div>
  );
}

export default Docs;
