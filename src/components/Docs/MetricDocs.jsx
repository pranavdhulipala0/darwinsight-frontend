import React, { useEffect, useState } from "react";
import CodeComponent from "../Common/CodeComponent";

const MetricDocs = ({ metric }) => {
  const [apiDocs, setApiDocs] = useState(null); // Initialize state with null

  const docs = [
    {
      metricType: "SEARCHMETRICS",
      codeTitle: "Snippet to generate Search Metrics",
      requestType: "POST",
      codeValue: ` const url = 'http://localhost:3000/api/services/searchmetrics'
const requestBody = {
    tenantId: 'your_tenant_id',
    applicationId: 'your_application_id',
    query: 'search query'
};

try {
    await fetch(url, {
    method: 'POST',
    headers: {
        'Content-Type': 'application/json'
    },
    body: JSON.stringify(requestBody)
    });
} catch (error) {
    console.error('Error:', error);
}`,
    },
    {
      metricType: "GEOMETRICS",
      codeTitle: "Snippet to generate Geo Metrics",
      requestType: "POST",
      codeValue: `try {
navigator.geolocation.getCurrentPosition(position => {
    const { latitude, longitude } = position.coords;
    try {
    fetch('http://localhost:3000/api/services/geometrics', {
        method: 'POST',
        headers: {
        'Content-Type': 'application/json'
        },
        body: JSON.stringify({
        "tenantId": "Your Tenant ID here",
        "applicationId": "Your application ID here",
        "latitude": latitude,
        "longitude": longitude
        })
    });
    } catch (fetchError) {
    console.error('Error occurred while fetching:', fetchError);
    }
});
} catch (geolocationError) {
console.error('Error occurred while getting geolocation:', geolocationError);
}`,
    },
    {
      metricType: "COUNTMETRICS",
      codeTitle: "Snippet to generate Count Metrics",
      requestType: "POST",
      codeValue: `const requestBody = {
tenantId: "Your Tenant ID here",
applicationId: "Your Application ID here"
};

try {
await fetch('http://localhost:3000/api/services/countmetrics', {
    method: 'POST',
    headers: {
    'Content-Type': 'application/json'
    },
    body: JSON.stringify(requestBody)
});
} catch (error) {
console.error('Error fetching data:', error);
}`,
    },
    {
      metricType: "PERFORMANCEMETRICS",
      codeTitle: "Snippet to generate Performance Metrics",
      requestType: "POST",
      codeValue: `const startTime = performance.now();
window.addEventListener("load", () => {
  const loadTime = performance.now() - startTime;
  fetch("http://localhost:5000/api/services/performancemetric", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      tenantId: "Your Tenant ID here",
      applicationId: "Your application ID here",
      performance: loadTime,
    }),
  });
});`,
    }
  ];

  useEffect(() => {
    const selectedDoc = docs.find((doc) => doc.metricType === metric);
    setApiDocs(selectedDoc);
  }, [metric]); // Include metric and docs in the dependency array

  return <div>{apiDocs && <CodeComponent data={apiDocs} />}</div>;
};

export default MetricDocs;
