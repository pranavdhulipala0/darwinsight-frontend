import React from "react";
import { Card } from "flowbite-react";
import { IoMdArrowForward } from "react-icons/io";

function WideBlock({ data }) {
  return (
    <Card href="#" className=" my-1">
      <div className="max-w-max">
        <p className="text-blue-700 text-md font-sans antialiased font-bold dark:text-white">
          {data.appName}
        </p>
        <p className="text-gray-700 dark:text-gray-400">{data.appUrl}</p>
      </div>
      {/* <IoMdArrowForward className="ml-auto" /> */}
    </Card>
  );
}

export default WideBlock;
