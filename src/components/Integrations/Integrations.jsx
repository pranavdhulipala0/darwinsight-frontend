import React, { useEffect, useState } from "react";
import WideBlock from "./WideBlock";
import IntegTable from "./IntegTable";
import { Button } from "flowbite-react";
import Modal from "./ModalComponent";
import ModalComponent from "./ModalComponent";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

function Integrations() {
  const headers = ["Integration", "Application URL", "Application ID", ""];
  const [formValues, setFormValues] = useState({
    applicationName: "",
    applicationUrl: "",
  });
  const [tenantId, setTenantId] = useState("");
  const [integrations, setIntegrations] = useState([]);
  const [openModal, setOpenModal] = useState(false);
  const [searchTerm, setSearchTerm] = useState("");

  const data = { heading: "Add a new integration", submitText: "Create" };

  async function finalFunction() {
    await createIntegration();
    setOpenModal(false);
  }

  useEffect(() => {
    if (tenantId) fetchIntegrations();
  }, [tenantId]);

  useEffect(() => {
    setTenantId(sessionStorage.getItem("tenantId"));
  }, []);

  async function fetchIntegrations() {
    const url = "http://localhost:5000/api/application/fetchIntegrations";
    try {
      const response = await fetch(url, {
        method: "POST",
        credentials: "include",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          tenantId: tenantId,
        }),
      });

      if (!response.ok) {
        toast.error("Failed to fetch Integrations");
        return;
      }
      const data = await response.json();
      setIntegrations(data);
    } catch (error) {
      toast.error("Error: " + error.message);
      console.error("Error fetching data:", error);
    }
  }

  async function createIntegration() {
    const url = "http://localhost:5000/api/application/createIntegration";
    try {
      const response = await fetch(url, {
        method: "POST",
        credentials: "include",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          tenantId: tenantId,
          applicationName: formValues.applicationName,
          applicationUrl: formValues.applicationUrl,
        }),
      });

      if (response.status === 400) {
        const errorData = await response.json();
        toast.error(errorData.message);
      } else if (response.ok) {
        const data = await response.json();
        toast.success(data.message);
      } else {
        toast.error("Unexpected Error");
      }
    } catch (error) {
      toast.error("Error: " + error.message);
    }
  }

  // Filter integrations based on search term
  const filteredIntegrations = integrations.filter((integration) =>
    integration.applicationName.toLowerCase().includes(searchTerm.toLowerCase())
  );

  return (
    <div className="flex flex-1 flex-col px-4">
      <ToastContainer />
      <section className="bg-white dark:bg-gray-900">
        <div className="py-8 px-4 mx-auto max-w-screen-xl text-center lg:py-10 pb-7">
          <h1 className="mb-4 px-3 text-2xl font-extrabold tracking-tight leading-none text-gray-900 md:text-5xl lg:text-6xl dark:text-white">
            All your integrations, in one place.
          </h1>
          <div className="flex flex-col space-y-4 sm:flex-row sm:justify-center sm:space-y-0">
            <a
              onClick={() => setOpenModal(true)}
              className="inline-flex justify-center items-center py-3 px-5 text-base font-medium text-center text-white rounded-lg bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 dark:focus:ring-blue-900"
            >
              New Integration
              <svg
                className="w-3.5 h-3.5 ms-2 rtl:rotate-180"
                aria-hidden="true"
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 14 10"
              >
                <path
                  stroke="currentColor"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="M1 5h12m0 0L9 1m4 4L9 9"
                />
              </svg>
            </a>
            <form className="flex items-center justify-center">
              <label htmlFor="simple-search" className="sr-only">
                Search for your integrations...
              </label>
              <div className="relative flex items-center">
                <input
                  type="text"
                  id="simple-search"
                  value={searchTerm}
                  onChange={(e) => setSearchTerm(e.target.value)}
                  className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full px-4 py-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  placeholder="Search for your integrations..."
                  required
                />
                <button
                  type="submit"
                  className="absolute inset-y-0 right-0 flex items-center justify-center px-3 text-gray-600 hover:text-gray-900 focus:outline-none"
                >
                  <svg
                    className="w-5 h-5"
                    aria-hidden="true"
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 20 20"
                  >
                    <path
                      stroke="currentColor"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth="2"
                      d="m19 19-4-4m0-7A7 7 0 1 1 1 8a7 7 0 0 1 14 0Z"
                    />
                  </svg>
                  <span className="sr-only">Search</span>
                </button>
              </div>
            </form>
          </div>
        </div>
      </section>
      <IntegTable data={filteredIntegrations} headers={headers} />
      <ModalComponent
        openModal={openModal}
        setOpenModal={setOpenModal}
        finalFunction={finalFunction}
        heading={data.heading}
        submitText={data.submitText}
        formValues={formValues}
        setFormValues={setFormValues}
      />
    </div>
  );
}

export default Integrations;
