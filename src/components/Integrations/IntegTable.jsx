import React, { useState, useEffect } from "react";
import WideBlock from "./WideBlock";
import ServicesModal from "../Services/ServicesModal";
import { Tooltip } from "flowbite-react";

function IntegTable({ data, headers }) {
  const [isOpen, setIsOpen] = useState(false);
  const [currId, setCurrId] = useState("");
  const [copiedText, setCopiedText] = useState("Copy to clipboard");

  function openModal(id) {
    setCurrId(id);
    setIsOpen(true);
  }

  const closeModal = () => {
    setCurrId("");
    setIsOpen(false);
  };

  const copyToClipboard = (text) => {
    navigator.clipboard
      .writeText(text)
      .then(() => {
        setCopiedText("Copied");
        setTimeout(() => {
          setCopiedText("Copy to clipboard");
        }, 1200); 
      })
      .catch((err) => {
        console.error("Unable to copy text to clipboard", err);
      });
  };

  useEffect(() => {
    const handleEscapeKey = (e) => {
      if (e.key === "Escape") {
        closeModal();
      }
    };

    document.addEventListener("keydown", handleEscapeKey);

    return () => {
      document.removeEventListener("keydown", handleEscapeKey);
    };
  }, []);

  return (
    <>
  {isOpen && (
    <ServicesModal
      isOpen={isOpen}
      closeModal={closeModal}
      applicationId={currId}
    />
  )}

  <div className="relative overflow-x-auto shadow-md sm:rounded-lg" style={{ maxHeight: "630px" }}>
    <table className="w-full text-sm text-left rtl:text-right text-gray-500 dark:text-gray-400">
      <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
        <tr>
          {headers &&
            headers.map((item, index) => (
              <th key={index} scope="col" className="px-6 py-3 bg-blue-100">
                {item}
              </th>
            ))}
        </tr>
      </thead>

      <tbody>
        {data &&
          data.map((item) => (
            <tr
              key={item.applicationId}
              className="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600"
            >
              <th
                scope="row"
                className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white"
              >
                {item.applicationName}
              </th>
              <td className="px-6 py-4">
                <div className="flex items-center">
                  <a
                    href={"https://" + item.applicationUrl}
                    rel="noreferrer"
                    target="_blank"
                  >
                    <u>{item.applicationUrl}</u>
                  </a>
                </div>
              </td>
             
                <td className="px-6 py-4 cursor-pointer" onClick={() => copyToClipboard(item.applicationId)}>
                <Tooltip content={copiedText} > {item.applicationId}{" "}</Tooltip>
                </td>
              
              <td
                onClick={() => {
                  openModal(item.applicationId);
                }}
                className="px-6 py-4 text-right cursor-pointer"
              >
                <svg
                  className="w-3.5 h-3.5 ms-2 rtl:rotate-180"
                  aria-hidden="true"
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 14 10"
                >
                  <path
                    stroke="currentColor"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth="2"
                    d="M1 5h12m0 0L9 1m4 4L9 9"
                  />
                </svg>{" "}
              </td>
            </tr>
          ))}
      </tbody>
    </table>
    {!data || data.length === 0 && <div className="flex justify-center text-md py-4">No integrations found</div>}

  </div>
</>

  );
}

export default IntegTable;
