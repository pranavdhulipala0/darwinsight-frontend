// ModalComponent.js

import React from "react";
import { Button, Modal } from "flowbite-react";

const ModalComponent = ({
  openModal,
  setOpenModal,
  finalFunction,
  heading,
  submitText,
  formValues,
  setFormValues,
}) => {
  const { applicationName, applicationUrl } = formValues;

  return (
    <div>
      <Modal show={openModal} onClose={() => setOpenModal(false)}>
        <Modal.Header>{heading}</Modal.Header>
        <div className="p-6 space-y-6">
          <div>
            <label className="block text-sm font-medium text-gray-700">
              Application Name
            </label>
            <input
              type="text"
              value={applicationName}
              onChange={(e) =>
                setFormValues({
                  ...formValues,
                  applicationName: e.target.value,
                })
              }
              className="mt-1 p-2 w-full border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-300"
              placeholder="Enter application name"
            />
          </div>
          <div>
            <label className="block text-sm font-medium text-gray-700">
              Application URL
            </label>
            <input
              type="text"
              value={applicationUrl}
              onChange={(e) =>
                setFormValues({ ...formValues, applicationUrl: e.target.value })
              }
              className="mt-1 p-2 w-full border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-300"
              placeholder="Enter application URL"
            />
          </div>
          <div className="flex justify-end">
            <Button
              className="flex-col bg-blue-700"
              onClick={() => {
                finalFunction();
              }}
            >
              {submitText}
            </Button>
          </div>
        </div>
        {/* <Modal.Footer className="flex justify-end">
        </Modal.Footer> */}
      </Modal>
    </div>
  );
};

export default ModalComponent;
