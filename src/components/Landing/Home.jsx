import { useEffect, useState } from "react";
import NavigBar from "../Common/NavigBar";
import SidebarComp from "../Common/SidebarComp";
import Dashboard from "./Dashboard";
import Integrations from "../Integrations/Integrations";
import Services from "../Services/Services";
import Docs from "../Docs/Docs";
import { useLocation } from "react-router-dom";
import Profile from "../Profile/Profile";
import Signout from "../Common/Signout";

function Home() {
  const location = useLocation();
  const [logged, setLogged] = useState(false);
  var tenantName = "";
  try {
    tenantName = location.state.tenantName;
  } catch (error) {
    tenantName = "ERROR";
  }
  const views = {
    // DASHBOARD: <Dashboard />,
    INTEGRATIONS: <Integrations />,
    SERVICES: <Services />,
    PROFILE: <Profile />,
    APIDOCS: <Docs />,
    SIGNOUT: <Signout />,
  };

  const [view, setView] = useState("INTEGRATIONS");

  useEffect(() => {
    if (sessionStorage.getItem("tenantName")) setLogged(true);
  }, []);

  // if(tenantName === 'ERROR'){
  //   return(<div className="flex flex-col items-center justify-content-center mx-auto mt-2 pt-6"><p>You aren't logged in</p></div>)
  // }
  return (
    <>
      {logged && (
        <>
          <NavigBar tenantName={tenantName} />
          <div className="flex scroll-smooth scroll-auto">
            <SidebarComp setView={setView} />
            {views[view]}
          </div>
        </>
      )}
    </>
  );
}

export default Home;
