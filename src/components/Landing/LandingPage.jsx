import React, { useEffect, useState } from "react";
import { Button, Checkbox, Label, TextInput } from "flowbite-react";
import { Link } from "react-router-dom";
import Login from "./Login";
import Register from "./Register";
import { useNavigate } from "react-router-dom";

const LandingPage = ({ isLoggedIn, setIsLoggedIn }) => {
  const [loginView, setLoginView] = useState(true);
  const navigate = useNavigate();

  useEffect(() => {
    if (isLoggedIn) {
      navigate("/home"); // Keep using navigate with state
    }
  }, []);

  return (
    <div className="flex flex-cols-2 justify-center items-center h-screen">
      <div className="">
        <img src="landinggif.gif" alt="Landing" className=" h-96" />
      </div>
      <div className="bg-gradient-to-t w-200 from-slate-100 to-cyan-100">
        <div className="max-w-xl mx-auto h-screen flex items-center justify-center p-4 pt-6">
          <div className="w-full min-h-screen flex flex-col sm:justify-center items-center pt-6 sm:pt-0">
            <div className="w-full sm:max-w-md p-5 shadow mx-auto bg-white rounded-md ">
              <img
                src="navbarlogo3.png"
                alt="API Track"
                className="h-20 pb-6 mx-auto"
              />
              {loginView ? (
                <Login
                  loginView={loginView}
                  setIsLoggedIn={setIsLoggedIn}
                  setLoginView={setLoginView}
                />
              ) : (
                <Register loginView={loginView} setLoginView={setLoginView} />
              )}
              <div
                className="mt-6 text-center"
                onClick={() => {
                  setLoginView(!loginView);
                }}
              >
                <a href="#" className="underline">
                  {loginView ? (
                    <>Sign up for an account</>
                  ) : (
                    <>Got an account? Log in.</>
                  )}
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default LandingPage;
