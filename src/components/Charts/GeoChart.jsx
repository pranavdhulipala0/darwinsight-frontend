import React, { useEffect, useState } from "react";
import Chart from "react-google-charts";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import ButtonGroupSetter from "../helpers/ButtonGroupSetter";
import Loader from "../helpers/Loader";
import colorsArray from "../helpers/colors.js";

const GeoChart = ({ applicationId, startDate, endDate, bool }) => {
  // const tenantId = "sAryt6U";
  const metricType = "GEOMETRIC";

  const [geoData, setGeoData] = useState();
  const [fetched, setFetched] = useState(false);
  const [chartType, setChartType] = useState("GeoChart");
  const [loading, setLoading] = useState(true); 
  const chartViews = [
    { name: "Geo View", type: "GeoChart" },
    { name: "Pie Distribution", type: "PieChart" },
    { name: "Bar View", type: "BarChart" },
    { name: "Column View", type: "ColumnChart" },
  ];

  const [tenantId, setTenantId] = useState('');

  useEffect(()=>{
    setTenantId(sessionStorage.getItem('tenantId'))
  },[])

  const options = {
    title: "Geo Metrics",
    curveType: "function",
    legend: { position: "bottom" },
    // displayMode: 'markers',
    // colorAxis: { colors: colorsArray},
    // defaultColor: "#f5f5f5",

    animation: {
      startup: true,
      easing: "out",
      duration: 1500,
    },
    enableInteractivity: true,
  };

  useEffect(() => {
    if (bool && tenantId) fetchData();
    console.log(geoData);
  }, [bool]);

  useEffect(() => {}, [fetched]);

  useEffect(() => {
    fetchData();
  }, [tenantId]);

  async function regularizeData(data) {
    if (
      !data ||
      (data && !data[0]) ||
      (data[0].GEOMETRIC.metrics.length === 0)
    ) {
      setGeoData(null);
    } else {
      const result = data.flatMap((item) => {
        return item.GEOMETRIC.metrics.map((metric) => [
          metric.location,
          metric.counts,
        ]);
      });
      result.unshift(["Location", "Popularity"]);
      console.log(result);
      setGeoData(result);
      // console.log("in regularize");
      console.log(data);
    }
  }

  async function fetchData() {
    // const tenantId = "sAryt6U";
    const url = "http://localhost:5000/api/service/fetchMetrics";
    try {
      const response = await fetch(url, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          tenantId: tenantId,
          applicationId: applicationId,
          metricType: metricType,
          startDate: startDate,
          endDate: endDate,
        }),
      });

      if (!response.ok) {
        toast.error("Failed to fetch Data");
        return;
      }
      const data = await response.json().then((response) => {
        regularizeData(response);
        setTimeout(() => setLoading(false), 2000); // Hide loader after 2 seconds
        setFetched(true);
      });
    } catch (error) {
      toast.error("Error: " + error.message);
      console.error("Error fetching data:", error);
    }
  }

  return loading ? (
    <Loader message = "Fetching data..."/> // Show loader while loading
  ) : geoData ? (
    <div>
      <ToastContainer /> {/* Set flex-1 here to make it take full width */}
      <ButtonGroupSetter
        data={chartViews}
        chartType={chartType}
        setChartType={setChartType}
      />
      <div className="py-5"></div>
      <Chart
        chartType={chartType}
        width="100%"
        height="500px"
        data={geoData}
        options={options}
      />
    </div>
  ) : (
    <div className="text-center mt-5">
      <p className="text-gray-800 py-2 text-center text-xl">No data found.</p>
      <p className="text-gray-600">
        If you have selected dates, make sure to select valid ranges.
      </p>
      <p className="text-gray-600">
        If you have no data irrespective of dates, make sure you've placed our
        API snippets properly.
      </p>
    </div>
  );
};

export default GeoChart;
