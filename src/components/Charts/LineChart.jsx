import React, { useEffect, useState } from "react";
import Chart from "react-google-charts";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import ButtonGroupSetter from "../helpers/ButtonGroupSetter";
import Loader from "../helpers/Loader";

const LineChart = ({ applicationId, startDate, endDate, bool }) => {
  // const tenantId = "sAryt6U";
  const metricType = "COUNTMETRIC";
  const [countData, setCountData] = useState();
  const [fetched, setFetched] = useState(false);
  const [chartType, setChartType] = useState("Line");
  const [loading, setLoading] = useState(true); // State to manage loading status
  const chartViews = [
    {
      name: "Line View",
      type: "Line",
    },
    {
      name: "Smooth Line View",
      type: "LineChart",
    },
    {
      name: "Bar View",
      type: "BarChart",
    },
    {
      name: "Column View",
      type: "ColumnChart",
    },
    {
      name: "Pie Distribution",
      type: "PieChart",
    },
    // {
    //   name: "Calendar View",
    //   type: "Calendar",
    // },
    {
      name: "Stepped Area View",
      type: "SteppedAreaChart",
    },
  ];

  const [tenantId, setTenantId] = useState('');

  useEffect(()=>{
  },[])

  const options = {
    title: "Application clicks",
    curveType: "function",
    legend: { position: "bottom" },
    animation: {
      startup: true,
      easing: "out",
      duration: 1500,
    },
    enableInteractivity: true,
  };

  useEffect(() => {
    if (bool) fetchData();
    console.log(countData);
  }, [bool]);

  useEffect(() => {}, [fetched]);

  useEffect(() => {
    setTenantId(sessionStorage.getItem('tenantId'))
    if(tenantId)fetchData();
  }, []);

  async function regularizeData(data) {
    if (!data || (data && !data[0])) {
      setCountData(null);
    } else {
      const result = data.map((item) => {
        const date = new Date(item.COUNTMETRIC.date);
        const formattedDate = date.toLocaleDateString("en-US", {
          month: "short",
          day: "2-digit",
        });
        return [formattedDate, item.COUNTMETRIC.counts];
      });
      result.unshift(["Date", "Clicks"]);
      setCountData(result);
      // console.log("in regularize");
      console.log(data);
    }
  }

  async function fetchData() {
    // const tenantId = "sAryt6U";
    const url = "http://localhost:5000/api/service/fetchMetrics";
    try {
      const response = await fetch(url, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          tenantId: tenantId,
          applicationId: applicationId,
          metricType: metricType,
          startDate: startDate,
          endDate: endDate,
        }),
      });

      if (!response.ok) {
        toast.error("Failed to fetch Data");
        return;
      }
      const data = await response.json().then((response) => {
        regularizeData(response);
        setTimeout(() => setLoading(false), 2000); // Hide loader after 2 seconds
        setFetched(true);
      });
    } catch (error) {
      toast.error("Error: " + error.message);
      console.error("Error fetching data:", error);
    }
  }

  return loading ? (
    <Loader message = "Fetching data..."/> // Show loader while loading
  ) : countData ? (
    <div>
      <ToastContainer /> {/* Set flex-1 here to make it take full width */}
      <ButtonGroupSetter
        data={chartViews}
        chartType={chartType}
        setChartType={setChartType}
      />
      <Chart
        chartType={chartType}
        width="100%"
        height="500px"
        data={countData}
        options={options}
      />
    </div>
  ) : (
    <div className="text-center mt-5">
      <p className="text-gray-800 py-2 text-center text-xl">No data found.</p>
      <p className="text-gray-600">
        If you have selected dates, make sure to select valid ranges.
      </p>
      <p className="text-gray-600">
        If you have no data irrespective of dates, make sure you've placed our
        API snippets properly.
      </p>
    </div>
  );
};

export default LineChart;
