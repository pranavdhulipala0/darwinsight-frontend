import React, { useEffect, useState } from "react";
import Chart from "react-google-charts";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import ButtonGroupSetter from "../helpers/ButtonGroupSetter";
import Loader from "../helpers/Loader";
import colorsArray from "../helpers/colors.js";
import Table from "../Common/Table.jsx";
import FlowbiteTable from "../Common/FlowbiteTable.jsx";

const WordChart = ({ applicationId, startDate, endDate, bool }) => {
  // const tenantId = "sAryt6U";
  const metricType = "SEARCHMETRIC";

  const [searchData, setSearchData] = useState();
  const [flatData, setFlatData] = useState();
  const [fetched, setFetched] = useState(false);
  const [chartType, setChartType] = useState("Table");
  const [isLoading, setIsLoading] = useState(true); // State to track loading
  const [tenantId, setTenantId] = useState("");

  const chartViews = [
    { name: "Table View", type: "Table" },
    { name: "Column View", type: "ColumnChart" },
    { name: "Bar View", type: "BarChart" },
    { name: "Trie View", type: "WordTree" },
    { name: "Pie Distribution", type: "PieChart" },
  ];

  const options = {
    title: "Search Metrics",
    curveType: "function",
    legend: { position: "bottom" },

    animation: {
      startup: true,
      easing: "out",
      duration: 1500,
    },
    enableInteractivity: true,
  };

  useEffect(() => {
    if (bool && tenantId) fetchData();
    console.log(searchData);
  }, [bool]);

  useEffect(() => {}, [fetched]);

  useEffect(() => {
    setTenantId(sessionStorage.getItem("tenantId"));
  }, []);
  useEffect(() => {
    if (tenantId) fetchData();
  }, [tenantId]);

  function regularizeData(data) {
    if (
      !data ||
      (data && !data[0]) ||
      data[0].SEARCHMETRIC.searchQueries.length === 0
    ) {
      setSearchData(null);
    } else {
      const searchQueries = data.flatMap(
        (item) => item.SEARCHMETRIC.searchQueries
      );

      const wordCountMap = new Map();
      searchQueries.forEach((query) => {
        if (wordCountMap.has(query)) {
          wordCountMap.set(query, wordCountMap.get(query) + 1);
        } else {
          wordCountMap.set(query, 1);
        }
      });

      const result = Array.from(wordCountMap.entries()).map(([word, count]) => [
        word,
        count,
      ]);
      result.unshift(["Query", "Frequency"]);

      const flatDataArray = Array.from(wordCountMap.keys()); // Changed variable name to avoid conflict
      setFlatData(flatDataArray); // Update flatData state directly

      setSearchData(result);
    }
  }

  async function fetchData() {
    // const tenantId = "sAryt6U";
    const url = "http://localhost:5000/api/service/fetchMetrics";
    try {
      const response = await fetch(url, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          tenantId: tenantId,
          applicationId: applicationId,
          metricType: metricType,
          startDate: startDate,
          endDate: endDate,
        }),
      });

      if (!response.ok) {
        toast.error("Failed to fetch Data");
        return;
      }
      const data = await response.json().then((response) => {
        regularizeData(response);
        setFetched(true);
        // Set loading state to false after 2 seconds
        setTimeout(() => {
          setIsLoading(false);
        }, 2000);
      });
    } catch (error) {
      toast.error("Error: " + error.message);
      console.error("Error fetching data:", error);
    }
  }

  return (
    <div>
      {isLoading ? (
        <Loader message="Fetching data..." />
      ) : searchData ? (
        <div>
          <ToastContainer />
          <ButtonGroupSetter
            data={chartViews}
            chartType={chartType}
            setChartType={setChartType}
          />
          <div className="py-5 flex justify-center text-center"></div>
          {chartType !== "Table" ? (
            <Chart
              chartType={chartType}
              width="100%"
              height="500px"
              data={searchData}
              options={options}
            />
          ) : (
            searchData && (
              <FlowbiteTable
                data={searchData}
                headers={["Query", "Frequency"]}
              />
            )
          )}
        </div>
      ) : (
        <div className="text-center mt-5">
          <p className="text-gray-800 py-2 text-center text-xl">
            No data found.
          </p>
          <p className="text-gray-600">
            If you have selected dates, make sure to select valid ranges.
          </p>
          <p className="text-gray-600">
            If you have no data irrespective of dates, make sure you've placed
            our API snippets properly.
          </p>
        </div>
      )}
    </div>
  );
};

export default WordChart;
