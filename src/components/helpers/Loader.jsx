import React from "react";
import { Spinner } from "flowbite-react";

const Loader = ({message}) => {
  return (
    <>
      <div className="flex justify-center pt-5 pb-3">
        <Spinner aria-label="Default status example" />
      </div>
      <p className="text-center text-gray-800">{message}</p>
    </>
  );
};

export default Loader;
