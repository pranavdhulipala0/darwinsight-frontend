import React, { useState } from "react";

const Datepicker = ({ date, setDate }) => {
  const [showPicker, setShowPicker] = useState(false);

  const handleDateChange = (event) => {
    const selectedValue = event.target.value; // Get the selected date value

    // Check if the selected value is a valid date (YYYY-MM-DD format)
    if (isValidDate(selectedValue)) {
      setDate(selectedValue); // Pass the selected date value to the onChange callback
      setShowPicker(false); // Hide the date picker
    } else {
      // Handle the case where the selected value is not a valid date
      // You can show an error message, provide a default value, or take any other appropriate action
      console.error("Invalid date selected:", selectedValue);
      // For example, if you want to set a default value:
      // onChange(DEFAULT_DATE_VALUE);
    }
  };

  const isValidDate = (dateString) => {
    const regex = /^\d{4}-\d{2}-\d{2}$/;
    return regex.test(dateString);
  };

  return (
    <div className="relative">
      <input
        type="date"
        value={date?date:''} // Use the selectedDate prop as the value of the input
        onChange={handleDateChange}
        className="block rounded-lg border-gray-300 w-full px-4 py-2 focus:outline-none"
      />
    </div>
  );
};

export default Datepicker;
