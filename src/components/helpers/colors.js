 const colorsArray = [
    "#c5f1c1",
    "#beefb8",
    "#b7ecaf",
    "#b0eaa5",
    "#a9e89c",
    "#a3e592",
    "#9de288",
    "#96e07e",
    "#90dd74",
    "#8ada69",
    "#84d85e",
    "#7ed552",
    "#79d246",
    "#73cf38",
    "#6dcc28"
];

export default colorsArray;

