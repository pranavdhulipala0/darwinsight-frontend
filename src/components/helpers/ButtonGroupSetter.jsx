import React from "react";
import { Button } from "flowbite-react";

const ButtonGroupSetter = ({ data, chartType, setChartType }) => {
  console.log(data);

  const handleButtonClick = (type) => {
    setChartType(type);
  };

  return (
    <div className="flex justify-center items-center">
    <Button.Group>
      {data.map((item, index) => (
        <Button
          key={index}
          onClick={() => handleButtonClick(item.type)}
          color={chartType === item.type ? "blue" : "gray"} // Change color to blue if item matches chartType
        >
          {item.name}
        </Button>
      ))}
    </Button.Group>
  </div>
  );
};

export default ButtonGroupSetter;
