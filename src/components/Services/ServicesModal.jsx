import React, { useEffect, useState } from "react";
import { Label, Tabs } from "flowbite-react";
import { HiAdjustments, HiClipboardList, HiUserCircle } from "react-icons/hi";
import { MdDashboard } from "react-icons/md";
import GeoChart from "../Charts/GeoChart";
import LineChart from "../Charts/LineChart";
import WordChart from "../Charts/WordChart";
import PerformanceChart from "../Charts/PerformanceChart";

import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Checkbox, Button, Modal } from "flowbite-react";
import Loader from "../helpers/Loader";
import Datepicker from "../helpers/Datepicker";

const ServicesModal = ({ isOpen, closeModal, applicationId }) => {
  const [applicationData, setApplicationData] = useState();
  const [selectDate, setSelectDate] = useState(false);
  const [startDate, setStartDate] = useState(null);
  const [endDate, setEndDate] = useState(null);
  const [fetched, setFetched] = useState(false);
  const [tenantId, setTenantId] = useState("");

  useEffect(() => {
    setTenantId(sessionStorage.getItem("tenantId"));
  }, []);

  useEffect(() => {
    if (tenantId) fetchApplicationData();
    // console.log(services);
  }, [tenantId]);

  useEffect(() => {
    if (tenantId) fetchApplicationData();
    console.log(services);
  }, [fetched]);

  async function fetchApplicationData() {
    // const tenantId = "sAryt6U";
    const url = "http://localhost:5000/api/application/fetchApplicationData";
    try {
      const response = await fetch(url, {
        method: "POST",
        credentials: "include",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          tenantId: tenantId,
          applicationId: applicationId,
        }),
      });

      if (!response.ok) {
        toast.error("Failed to fetch Integrations");
        return;
      }
      const data = await response.json();
      setApplicationData(data);
      setFetched(true);
    } catch (error) {
      toast.error("Error: " + error.message);
      console.error("Error fetching data:", error);
    }
  }

  const handleCheckboxChange = () => {
    setSelectDate((prevSelectDate) => !prevSelectDate);
  };

  const services = [
    {
      service: "Count Metrics",
      serviceId: "COUNTMETRIC",
      serviceComponent: (
        <LineChart
          metricType="COUNTMETRIC"
          applicationId={applicationId}
          startDate={startDate}
          endDate={endDate}
          bool={fetched}
        />
      ),
    },
    {
      service: "Search Metrics",
      serviceId: "SEARCHMETRIC",
      serviceComponent: (
        <WordChart
          applicationId={applicationId}
          startDate={startDate}
          endDate={endDate}
          bool={fetched}
        />
      ),
    },
    {
      service: "Geo Metrics",
      serviceId: "GEOMETRIC",
      serviceComponent: (
        <GeoChart
          applicationId={applicationId}
          startDate={startDate}
          endDate={endDate}
          bool={fetched}
        />
      ),
    },
    {
      service: "Performance Metrics",
      serviceId: "PERFORMANCEMETRIC",
      serviceComponent: (
        <PerformanceChart
          metricType="PERFORMANCEMETRIC"
          applicationId={applicationId}
          startDate={startDate}
          endDate={endDate}
          bool={fetched}
        />
      ),
    },
  ];

  // if(!fetched){
  //   return <Loader />
  // }
  // else{
  return (
    applicationData && (
      <div className="">
        <Modal size="7xl" show={isOpen} onClose={closeModal}>
          <Modal.Header>{applicationData.applicationName}</Modal.Header>

          <div className="p-6 space-y-6 overflow-y-auto max-h-96">
            {/* max-h-96 sets a maximum height for the modal body, and overflow-y-auto adds a scrollbar when the content overflows */}
            <div className="flex items-center gap-2">
              <Checkbox
                id="remember"
                checked={selectDate}
                onChange={handleCheckboxChange}
              />
              <Label htmlFor="remember">Select date ranges</Label>
            </div>

            {selectDate && (
              <>
                <div className="grid grid-cols-1 gap-2">
                  <div className="grid grid-cols-2 gap-2">
                    <label>Pick start date</label>
                    <label>Pick end date</label>
                    <Datepicker date={startDate} setDate={setStartDate} />
                    <Datepicker date={endDate} setDate={setEndDate} />
                  </div>
                  <div className="grid grid-cols-2 gap-2">
                    <Button
                      className="flex justify-center"
                      onClick={() => {
                        setStartDate(null);
                        setEndDate(null);
                        setFetched(false);
                      }}
                    >
                      Reset Dates
                    </Button>
                    <Button
                      color="blue"
                      className="flex justify-center"
                      onClick={() => setFetched(false)}
                    >
                      Filter
                    </Button>
                  </div>
                </div>
              </>
            )}

            <Tabs aria-label="Tabs with icons" style="underline">
              {services.map((service) => (
                <Tabs.Item key={service.serviceId} title={service.service}>
                  {service.serviceComponent}
                </Tabs.Item>
              ))}
            </Tabs>
          </div>
        </Modal>
      </div>
    )
  );
  // }
};

export default ServicesModal;
