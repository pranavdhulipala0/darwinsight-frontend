import React, { useEffect, useState } from "react";
import { FaLaptopCode } from "react-icons/fa";
import Block from "../Common/Block";
import { TbReportSearch } from "react-icons/tb";
import { MdOutlineShareLocation } from "react-icons/md";


function Services() {
  const services = {
    COUNTER: {
      title: "Count Metrics",
      description: "A tool to measure daily clicks on your site!",
      icon: <FaLaptopCode className="mx-auto h-20 w-20" />,
    },
    SEARCHER: {
      title: "Search Metrics",
      description: "A tool to track searches on your application!",
      icon: <TbReportSearch className="mx-auto h-20 w-20 " />,
    },
    LOCATOR: {
      title: "Geo Metrics",
      description: "A tool to track your audience!",
      icon: <MdOutlineShareLocation className="mx-auto h-20 w-20 " />,
    },
    
  };

  const [searchTerm, setSearchTerm] = useState('');
  const [searchResults, setSearchResults] = useState([]);

  const handleSearchChange = (e) => {
    setSearchTerm(e.target.value);
    searchServices(e.target.value);
  };

  const searchServices = (query) => {
    const results = Object.keys(services).filter((key) =>
      services[key].title.toLowerCase().includes(query.toLowerCase())
    );
    setSearchResults(results);
  };

  useEffect(()=>{
    searchServices(searchTerm)
    console.log(searchResults)
  },[searchTerm])
  return (
    <div className="flex flex-1 flex-col px-4 ">
      {" "}
      {/* Set flex-1 here to make it take full width */}
      <div className="border-3 rounded-xl bg-gray-400">
        <section className="bg-indigo-50 border-1 rounded-xl dark:bg-gray-900">
          <div className="px-4 mx-auto max-w-screen-xl text-center py-8">
            <h1 className="mb-4 text-4xl font-extrabold tracking-tight leading-none text-gray-900 md:text-5xl lg:text-6xl dark:text-white">
              All the essentials you need for your app.
            </h1>
            <p className="mb-8 text-lg font-normal text-gray-500 lg:text-xl sm:px-16 lg:px-48 dark:text-gray-400">
              Discover our services that help power various analytics for your
              application.
            </p>
            <div className="flex flex-col space-y-4 sm:flex-row sm:justify-center sm:space-y-0">
          
              <form className="flex items-center justify-center">
              <label htmlFor="simple-search" className="sr-only">
                Search for your services...
              </label>
              <div className="relative flex items-center">
                <input
                  type="text"
                  id="simple-search"
                  value={searchTerm}
                  onChange={(e) => setSearchTerm(e.target.value)}
                  className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full px-4 py-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  placeholder="Search for your services..."
                  required
                />
                <button
                  type="submit"
                  className="absolute inset-y-0 right-0 flex items-center justify-center px-3 text-gray-600 hover:text-gray-900 focus:outline-none"
                >
                  <svg
                    className="w-5 h-5"
                    aria-hidden="true"
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 20 20"
                  >
                    <path
                      stroke="currentColor"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth="2"
                      d="m19 19-4-4m0-7A7 7 0 1 1 1 8a7 7 0 0 1 14 0Z"
                    />
                  </svg>
                  <span className="sr-only">Search</span>
                </button>
              </div>
            </form>
            </div>
          </div>
        </section>
      </div>
      {/* <h1 className="text-lg">Check out our services!</h1> */}
      <div className="flex grid grid-cols-3 justify-center py-2">
        {searchResults.map((key) => (
          <div key={key}>
            <Block data={services[key]} />
          </div>
        ))}
      </div>
    </div>
  );
}

export default Services;
