import React from "react";
import Chart from "react-google-charts";

const Visualization = () => {
   const data = [
    ["Year", "Clicks"],
    ["2004", 400],
    ["2005", 460],
    ["2006", 1120],
    ["2007", 540],
  ];
  
   const options = {
    title: "Company Performance",
    curveType: "function",
    legend: { position: "bottom" },
  };

  return  <Chart
  chartType="LineChart"
  width="100%"
  height="400px"
  data={data}
  options={options}
/>;
};

export default Visualization;
