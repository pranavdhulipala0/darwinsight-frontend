import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import Loader from "../helpers/Loader";

function Signout() {
  const [loading, setLoading] = useState(true);
  const navigate = useNavigate();

  useEffect(() => {
    const signout = async () => {
      // Simulate a delay to show the loader
      await new Promise((resolve) => setTimeout(resolve, 2000));

      // Clear cookies
      document.cookie.split(";").forEach((c) => {
        document.cookie = c
          .replace(/^ +/, "")
          .replace(/=.*/, `=;expires=${new Date().toUTCString()};path=/`);
      });

      // Clear sessionStorage
      sessionStorage.clear();

      // Set loading to false to hide the loader
      setLoading(false);

      // Navigate to "/"
      navigate("/login");
    };

    signout();
  }, []);

  return (
    <div className="flex h-screen mx-auto items-center justify-center">
      {loading ? (
        <div>
          <Loader message="Logging you out..." />
        </div>
      ) : null}
    </div>
  );
}

export default Signout;
