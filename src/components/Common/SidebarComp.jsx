import React from "react";
import { Sidebar } from "flowbite-react";
import {
  HiArrowSmRight,
  HiChartPie,
  HiViewBoards,
  HiShoppingBag,
  HiUser,
} from "react-icons/hi";
import { HiMiniDocumentChartBar } from "react-icons/hi2";


function SidebarComp({ setView }) {
  const views = [
    // { view: "DASHBOARD",name:"Dashboard", icon: HiChartPie },
    { view: "INTEGRATIONS", name:"Integrations", icon: HiViewBoards },
    { view: "SERVICES", name:"Services", icon: HiShoppingBag },
    { view: "APIDOCS", name:"API Docs", icon: HiMiniDocumentChartBar},
    { view: "PROFILE", name: "Profile", icon: HiUser },
    { view: "SIGNOUT", name:"Logout", icon: HiArrowSmRight},


  ];

  return (
    <>
      <Sidebar className="float-left h-screen cursor-pointer bg-white shadow-md">
        <Sidebar.Items className="flex-grow">
          <Sidebar.ItemGroup>
            {views.map((view) => (
              <Sidebar.Item icon={view.icon} key={view.name}>
                <div onClick={() => setView(view.view)}>{view.name}</div>
              </Sidebar.Item>
            ))}
          </Sidebar.ItemGroup>
        </Sidebar.Items>
      </Sidebar>
    </>
  );
}

export default SidebarComp;
