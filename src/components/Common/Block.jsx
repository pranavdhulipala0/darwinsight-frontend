import React from "react";
import { Card } from "flowbite-react";

function Block({ data }) {
  return (
    <Card
      className="flex w-30 items-center h-30 justify-content-center text-center cursor-pointer mx-1 my-1"
    >
      {data.icon}
      <div className="flex flex-col">
        <h5 className="text-md font-bold tracking-tight text-gray-900 dark:text-white">
          {data.title}
        </h5>
        <p className="text-sm text-gray-700 dark:text-gray-400">
          {data.description}
        </p>
      </div>
    </Card>
  );
}

export default Block;
