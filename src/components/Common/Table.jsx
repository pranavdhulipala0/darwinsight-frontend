import React, { useEffect, useMemo, useState } from "react";
import { AgGridReact } from "ag-grid-react";
import "ag-grid-community/styles/ag-grid.css";
import "ag-grid-community/styles/ag-theme-quartz.css";

function Table({ data, headers }) {
  const [gridApi, setGridApi] = useState(null);
  const [rowData, setRowData] = useState(null);

  useEffect(() => {
    if (data && data.length > 0) {
      const rowDataJson = data.slice(1).map(([query, frequency]) => ({
        query,
        frequency,
      }));
      setRowData(rowDataJson);
    }
  }, [data]);

  const autoSizeStrategy = useMemo(() => {
    return {
      type: "fitGridWidth",
      defaultMinWidth: 100,
    };
  }, []);

  console.log("rowData:", rowData);
  console.log(
    "columnDefs:",
    headers.map((header) => ({ headerName: header, field: header }))
  );

  const onGridReady = (params) => {
    setGridApi(params.api);
    params.api.sizeColumnsToFit();
  };

  useEffect(() => {
    // console.log("GRID API CHANGED", gridApi);
  }, [gridApi]);

  useEffect(() => {
    console.log(rowData);
  }, [rowData]);

  const gridOptions = {
    pagination: true,
    paginationPageSize: 20,
    suppressRowClickSelection: false,
    domLayout: "autoHeight",
    copyHeadersToClipboard: true,
    enableCellTextSelection: true,
    getRowStyle: (params) => {
      return { color: "#000000" }; // Black color for data text
    },
    excelStyles: [
      {
        id: "header",
        interior: {
          color: "#000000",
          pattern: "Solid",
        },
        font: {
          size: 14,
          color: "#000000",
          weight: "bold",
        },
      },
    ],
  };

  return (
    <div className="ag-theme-quartz" style={{ width: "100%" }}>
      {rowData && (
        <AgGridReact
          onGridReady={onGridReady}
          gridOptions={gridOptions}
          rowData={rowData}
          autoSizeStrategy={autoSizeStrategy}
          columnDefs={headers.map((header) => ({
            headerName: header,
            field: header,
          }))}
        />


      )}
    </div>
  );
}

export default Table;
