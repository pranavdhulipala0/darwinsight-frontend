import React, { useState } from "react";
import SyntaxHighlighter from 'react-syntax-highlighter';
import { docco } from 'react-syntax-highlighter/dist/esm/styles/hljs'

const CodeComponent = ({ data }) => {
  const { codeTitle, requestType, codeValue } = data;
  const [copied, setCopied] = useState(false);

  const copyCodeToClipboard = () => {
    // Copy codeValue to clipboard
    navigator.clipboard.writeText(codeValue);
    setCopied(true);
    setTimeout(() => {
      setCopied(false);
    }, 2000); // 2 seconds timeout
  };
  
  return (
    <div className="border-1 rounded-xl pt-5 bg-gray-100 my-3 w-full"> {/* Change w-256 to w-full */}
      <div className="flex flex-row justify-between">
        <div className="flex flex-row">
          <p className="text-md pl-3 ml-1 py-1">{codeTitle}</p>
          <p className="text-md mt-1 ml-3 text-gray-400">[{requestType}]</p>
        </div>
        <p className="text-sm text-gray-600 mt-1 mr-3 underline cursor-pointer" onClick={copyCodeToClipboard}>
          {copied ? "Copied" : "Copy code"}
        </p>
      </div>
      <div className="mt-2 p-1 shadow" style={{ maxWidth: "100%" }}>
        <SyntaxHighlighter language="javascript" className="w-full" style={docco}>
          {codeValue}
        </SyntaxHighlighter>
      </div>
    </div>
  );
};

export default CodeComponent;
