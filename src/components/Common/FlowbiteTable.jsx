import React, { useEffect, useState } from "react";
import { Table } from "flowbite-react"; // Assuming Flowbite provides a Table component

function FlowbiteTable({ data, headers }) {
  const [rowData, setRowData] = useState(data);
  const [sortedData, setSortedData] = useState(data);
  const [sortDirection, setSortDirection] = useState("asc");
  const [searchTerm, setSearchTerm] = useState("");

  useEffect(() => {
    setRowData(data);
    setSortedData(data);
  }, [data]);

  useEffect(() => {
    console.log(rowData);
  }, [rowData]);

  const handleSort = () => {
    const newSortDirection = sortDirection === "asc" ? "desc" : "asc";
    const sorted = [...sortedData].sort((a, b) => {
      if (sortDirection === "asc") {
        return a[1] - b[1];
      } else {
        return b[1] - a[1];
      }
    });
    setSortedData(sorted);
    setSortDirection(newSortDirection);
  };

  // Function to handle search
  const handleSearch = (e) => {
    const searchValue = e.target.value.toLowerCase();
    setSearchTerm(searchValue);
    const filteredData = rowData.filter((item) =>
      item[0].toLowerCase().includes(searchValue)
    );
    setSortedData(filteredData);
  };

  return (
    <div>
      <form className="flex items-center justify-center">
        <label htmlFor="simple-search" className="sr-only">
          Search for your queries...
        </label>
        <div className="relative flex items-center">
          <input
            type="text"
            id="simple-search"
            value={searchTerm}
            onChange={handleSearch}
            className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full px-4 py-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
            placeholder="Search for queries..."
            required
          />
          <button
            type="submit"
            className="absolute inset-y-0 right-0 flex items-center justify-center px-3 text-gray-600 hover:text-gray-900 focus:outline-none"
          >
            <svg
              className="w-5 h-5"
              aria-hidden="true"
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 20 20"
            >
              <path
                stroke="currentColor"
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                d="m19 19-4-4m0-7A7 7 0 1 1 1 8a7 7 0 0 1 14 0Z"
              />
            </svg>
            <span className="sr-only">Search</span>
          </button>
        </div>
      </form>
      {rowData && (
        <Table>
          <Table.Head>
            <Table.HeadCell>Search Query</Table.HeadCell>
            <Table.HeadCell onClick={handleSort}>
              Frequency {sortDirection === "asc" ? "▲" : "▼"}
            </Table.HeadCell>
          </Table.Head>
          <Table.Body className="divide-y">
            {sortedData.map((item, index) =>
              index > 0 ? (
                <Table.Row key={index}>
                  <Table.Cell className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                    {item[0]}
                  </Table.Cell>
                  <Table.Cell>{item[1]}</Table.Cell>
                </Table.Row>
              ) : (
                <></>
              )
            )}
          </Table.Body>
        </Table>
      )}
    </div>
  );
}

export default FlowbiteTable;
