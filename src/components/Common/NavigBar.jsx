import React from "react";
import { Navbar } from "flowbite-react";
import { Avatar } from "flowbite-react";

function NavigBar() {
  return (
    <div>
      <div className="pt-2">
        <Navbar fluid rounded>
          <img src="navbarlogo3.png" alt="API Track" className="h-8" />
          <Navbar.Toggle />
          <Navbar.Collapse className="">
            {/* <p className="mt-1 mx-2 text-gray-800">{sessionStorage.getItem("tenantName").toUpperCase()}</p> */}
            {/* <Avatar img="https://mir-s3-cdn-cf.behance.net/project_modules/disp/ea7a3c32163929.567197ac70bda.png" placeholderInitials = {sessionStorage.getItem('tenantName')[0]} rounded bordered color="light" > */}
            <Avatar  placeholderInitials = {sessionStorage.getItem('tenantName')[0].toUpperCase()} rounded bordered color="blue" >
              <div className="space-y-1 text-md font-medium dark:text-white">
                <div>{sessionStorage.getItem('tenantName')}</div>
                <div className="text-sm text-gray-500 dark:text-gray-400 cursor-pointer underline">
                  {sessionStorage.getItem('tenantId')}
                </div>
              </div>
            </Avatar>
            {/* <img
              className="h-8 w-8 rounded-md object-cover object-center"
              alt="Image placeholder"
              src="https://st2.depositphotos.com/1104517/11965/v/950/depositphotos_119659092-stock-illustration-male-avatar-profile-picture-vector.jpg"
            /> */}
          </Navbar.Collapse>
        </Navbar>
      </div>
    </div>
  );
}

export default NavigBar;
