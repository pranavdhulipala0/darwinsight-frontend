import React, { useEffect, useState } from "react";

function Profile() {
  const [showButtons, setShowButtons] = useState(false);
  const [tenantId, setTenantId] = useState();
  const [tenantName, setTenantName] = useState();

  useEffect(() => {
    const storedTenantId = sessionStorage.getItem("tenantId");
    const storedTenantName = sessionStorage.getItem("tenantName");

    setTenantId(storedTenantId);
    setTenantName(storedTenantName);
  }, []);

  return (
    <div className="w-full px-6 mx-2 ">
      <div className="w-full px-4 mb-8">
        <div className="border-3 rounded-xl bg-gray-400">
          <section className="bg-white border-1 rounded-xl dark:bg-gray-900">
            <div className="px-4 mx-auto max-w-screen-xl text-center py-8">
              <h1 className="mb-4 text-4xl font-extrabold tracking-tight leading-none text-gray-900 md:text-5xl lg:text-6xl dark:text-white">
                Account Details
              </h1>
              <p className=" text-lg font-normal text-gray-500 lg:text-xl sm:px-16 lg:px-48 dark:text-gray-400">
                View & update your account details here.
              </p>
            </div>
          </section>
        </div>
      </div>
      <form className="m-2 p-2 w-full">
        <div className="space-y-8">
          {/* Profile Section */}
          <div className="border-b border-gray-900/10 pb-8">
            <h2 className="text-lg font-semibold leading-6 text-gray-900 mb-2">
              User details
            </h2>

            <div className="mt-6 grid grid-cols-2 sm:grid-cols-2 gap-6">
              <div>
                <label
                  htmlFor="username"
                  className="block text-sm font-medium text-gray-900"
                >
                  Tenant Name
                </label>
                <input
                  type="text"
                  name="username"
                  id="username"
                  autoComplete="username"
                  className="mt-1 block w-full border-gray-300 rounded-md shadow-sm py-1.5 px-2 focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                  placeholder={tenantName}
                  value={tenantName}
                  readOnly={!showButtons} // Set readOnly based on showButtons state
                />
              </div>
              <div>
                <label
                  htmlFor="username"
                  className="block text-sm font-medium text-gray-900"
                >
                  Tenant ID
                </label>
                <input
                  type="text"
                  name="tenantID"
                  id="tenantID"
                  autoComplete="tenantID"
                  className="mt-1 block w-full border-gray-300 rounded-md shadow-sm py-1.5 px-2 focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                  placeholder={tenantId}
                  value={tenantId}
                  readOnly={!showButtons} // Set readOnly based on showButtons state
                />
              </div>
              {/* Add more profile fields here */}
            </div>
          </div>

          {/* Personal Information Section */}
          <div className="border-b border-gray-900/10 pb-8">
            <h2 className="text-lg font-semibold leading-6 text-gray-900 mb-2">
              Personal Information
            </h2>
            <p className="text-sm text-gray-600">
              Use a permanent address where you can receive mail.
            </p>

            <div className="mt-6 grid grid-cols-1 sm:grid-cols-2 gap-6">
              <div>
                <label
                  htmlFor="first-name"
                  className="block text-sm font-medium text-gray-900"
                >
                  First name
                </label>
                <input
                  type="text"
                  name="first-name"
                  id="first-name"
                  autoComplete="given-name"
                  className="mt-1 block w-full border-gray-300 rounded-md shadow-sm py-1.5 px-2 focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                  readOnly={!showButtons} // Set readOnly based on showButtons state
                />
              </div>
              <div>
                <label
                  htmlFor="last-name"
                  className="block text-sm font-medium text-gray-900"
                >
                  Last name
                </label>
                <input
                  type="text"
                  name="last-name"
                  id="last-name"
                  autoComplete="family-name"
                  className="mt-1 block w-full border-gray-300 rounded-md shadow-sm py-1.5 px-2 focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                  readOnly={!showButtons} // Set readOnly based on showButtons state
                />
              </div>
              <div>
                <label
                  htmlFor="email"
                  className="block text-sm font-medium text-gray-900"
                >
                  Email address
                </label>
                <input
                  type="email"
                  name="email"
                  id="email"
                  autoComplete="email"
                  className="mt-1 block w-full border-gray-300 rounded-md shadow-sm py-1.5 px-2 focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                  readOnly={!showButtons} // Set readOnly based on showButtons state
                />
              </div>
              <div>
                <label
                  htmlFor="country"
                  className="block text-sm font-medium text-gray-900"
                >
                  Country
                </label>
                <select
                  id="country"
                  name="country"
                  autoComplete="country-name"
                  className="mt-1 block w-full border-gray-300 rounded-md shadow-sm py-1.5 px-2 focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                  readOnly={!showButtons} // Set readOnly based on showButtons state
                >
                  <option>United States</option>
                  <option>Canada</option>
                  <option>Mexico</option>
                </select>
              </div>
              <div>
                <label
                  htmlFor="street-address"
                  className="block text-sm font-medium text-gray-900"
                >
                  Street address
                </label>
                <input
                  type="text"
                  name="street-address"
                  id="street-address"
                  autoComplete="street-address"
                  className="mt-1 block w-full border-gray-300 rounded-md shadow-sm py-1.5 px-2 focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                  readOnly={!showButtons} // Set readOnly based on showButtons state
                />
              </div>
              <div>
                <label
                  htmlFor="city"
                  className="block text-sm font-medium text-gray-900"
                >
                  City
                </label>
                <input
                  type="text"
                  name="city"
                  id="city"
                  autoComplete="address-level2"
                  className="mt-1 block w-full border-gray-300 rounded-md shadow-sm py-1.5 px-2 focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                  readOnly={!showButtons} // Set readOnly based on showButtons state
                />
              </div>
              <div>
                <label
                  htmlFor="region"
                  className="block text-sm font-medium text-gray-900"
                >
                  State / Province
                </label>
                <input
                  type="text"
                  name="region"
                  id="region"
                  autoComplete="address-level1"
                  className="mt-1 block w-full border-gray-300 rounded-md shadow-sm py-1.5 px-2 focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                  readOnly={!showButtons} // Set readOnly based on showButtons state
                />
              </div>
              <div>
                <label
                  htmlFor="postal-code"
                  className="block text-sm font-medium text-gray-900"
                >
                  ZIP / Postal code
                </label>
                <input
                  type="text"
                  name="postal-code"
                  id="postal-code"
                  autoComplete="postal-code"
                  className="mt-1 block w-full border-gray-300 rounded-md shadow-sm py-1.5 px-2 focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                  readOnly={!showButtons} // Set readOnly based on showButtons state
                />
              </div>
            </div>
          </div>
        </div>

        {/* Buttons */}
        {showButtons ? (
          <div className="mt-6 flex items-center justify-end">
            <button
              type="button"
              className="text-sm mx-2 font-semibold text-gray-900"
              onClick={() => setShowButtons(false)}
            >
              Cancel
            </button>
            <button
              type="submit"
              className="ml-4 inline-flex items-center mx-2 px-4 py-2 border border-transparent text-sm font-medium rounded-md shadow-sm text-white bg-blue-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500"
            >
              Save
            </button>
          </div>
        ) : (
          <div className="mt-6 flex items-center justify-end">
            <button
              className="ml-4 inline-flex items-center mx-2 px-4 py-2 border border-transparent text-sm font-medium rounded-md shadow-sm text-white bg-blue-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500"
              onClick={() => setShowButtons(true)}
            >
              Update details
            </button>
          </div>
        )}
      </form>
    </div>
  );
}

export default Profile;
